# Open Match Test Cluster

A test cluster for the Open-Match matchmaker which contains of two namespaces where the first one holds a complete Open-Match core installation and the second one a user environment for user installed services.

The namespaces are as follows and contain the following services:

- open-match
    - Open-Match Core Backend Service
    - Open-Match Core Frontend Service
    - Open-Match Core Query Service
    - Open-Match Core Default Evaluator
    - Open-Match Core Redis Node
    - Open-Match Core Swagger UI
    - Open-Match Core Synchronizer
- open-match-demo
    - Gameye integrated Director
    - Match Function
    - Proxy service for Open-Match Core's frontend service for LatencyGG support

## Getting Started

The `/kubernetes/` directory contains nested directories for every namespace for the open-match test cluster.

Initiate with starting up Minikube:

```sh
minikube start
```

Followed by deploying the kubernetes deployment files via kustomize:

```sh
kubectl apply --kustomize ./kubernetes/open-match-core
```

And a match evaluator is also needed:

```sh
kubectl apply --kustomize ./kubernetes/open-match-evaluator
```

Now the demo namespace can be installed:

```sh
kubectl apply --kustomize ./kubernetes/open-match-demo
```

However, the proxy service-and the director require access to LatencyGG and Gameye respectively, which we can provide the credentials for through environment variables. They are to be stored as cluster secrets:

```sh
kubectl create secret generic om-director -n open-match-demo \
    --from-literal=GAMEYE_API_KEY=${GAMEYE_API_KEY}
```

```sh
kubectl create secret generic lgg-open-match-proxy -n open-match-demo \
    --from-literal=LGG_CLIENT_USERNAME=${LGG_CLIENT_USERNAME} \
    --from-literal=LGG_CLIENT_PASSWORD=${LGG_CLIENT_PASSWORD}
```

Now that the secret's been created, the proxy-and director will be able to start on retry. Otherwise, a manual restart should be invoked:

```sh
kubectl rollout restart deployments/lgg-open-match-proxy -n open-match-demo
kubectl rollout restart deployments/lgg-open-match-director -n open-match-demo
```

## License

This director service is available under the terms of the [WTFPL](http://www.wtfpl.net/) license. The full license is available in the `LICENSE` file.